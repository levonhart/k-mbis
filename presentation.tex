\RequirePackage[dvipsnames]{xcolor} % solve bug: cannot compile on ubuntu
\documentclass[10pt,xcolor={dvipsnames}]{beamer}
\input{lib/preamble}

\title[Maximum $2$-balanced induced (spanning) subgraph] % [] is optional - is placed on the bottom of the sidebar on every slide
{ % is placed on the title page
	\textbf{Maximum $2$-balanced induced (spanning) subgraph}
}

% \subtitle[The Feather Beamer Theme]
% {
%       \textbf{v. 1.1.0}
% }

\author[Leonardo de Abreu (presenting), Manoel Campêlo] %, \textit{Orientador:} Manoel Campêlo]
{     {Leonardo de Abreu} \\
	  {\ttfamily leonardo.abreu@lia.ufc.br} \\[1em]
      {Manoel Campêlo} \\
	  {\ttfamily mcampelo@ufc.br}%
}

\institute[UFC]
{%
	CNPq Inter Project\\
	Optimization problems in signed graphs with applications in social networks \\[2em]
	\textbf{Universidade Federal do Ceará}\\
	ParGO research Group %Grupo de Pesquisa em Paralelismo, Grafos e Otimização (ParGO)
}

% \date{\today}
\date{May 9, 2024}

%-------------------------------------------------------
% THE BODY OF THE PRESENTATION
%-------------------------------------------------------

\begin{document}

% \setbeamertemplate{background} 
% {
%     \includegraphics[width=\paperwidth,height=\paperheight]{figures/background-ufc.png}
% }

%-------------------------------------------------------
% THE TITLEPAGE
%-------------------------------------------------------

{\1% % this is the name of the PDF file for the background
	\begin{frame}[plain, noframenumbering] % the plain option removes the header from the title page, noframenumbering removes the numbering of this frame only
		\titlepage % call the title page information from above

\end{frame}}

\begin{frame}{Sumário}
	\tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}{Introduction}{Signed graph}
	A signed graph is a pair $(G,S)$, where $S \subset E(G)$ is the set of negative edges of $G$,
	called the \textit{signature} of $G$.

	\begin{minipage}[h][12em][c]{\textwidth}
		\centering%
		\only<1>{
			\vspace{3em}
			\input{figures/bis-ex.tex}
			\captionof{figure}{a signed graph}%
		}
		\label{fig:bis-instance}
	\end{minipage}
\end{frame}

\begin{frame}{Introduction}{2-balanced graph}
	\begin{block}{2-balanced graph}
		$(G,S)$ is \textit{balanced} if there is a partition $V=U \cup \bar{U}$, such that
		\[ S = [U,\bar{U}] .\]
	\end{block}
	\begin{minipage}[h][8em][c]{\textwidth}
		\centering%
		\input{figures/bal-ex.tex}
	\end{minipage}
\end{frame}

\begin{frame}{Introduction}{Maximum 2-balanced induced subgraph}
	\begin{problema}[2-\textsc{MBIS}]
		\begin{tabular}[h]{l p{0.75\textwidth}}
			\textsc{Input:} & Signed graph $G$ and weights $w(v)$ for each vertex $v$. \\
			\textsc{Objective:} & Find $B \subset V(G)$	that induces a balanced subgraph
			that maximizes $\sum_{u \in B} w(u)$,
		\end{tabular}
	\end{problema}
	\begin{minipage}[h][12em][c]{\textwidth}
		\footnotesize \centering%
		\only<2->{
			\input{figures/bis-ex-sol.tex}
			\captionof{figure}{Instance of 2-MBIS with all weights equal to 1.}%
		}
		\label{fig:bis-instance-sol}
	\end{minipage}
\end{frame}

\begin{frame}{Introduction}{Maximum 2-balanced spanning subgraph}
	\begin{problema}[2-\textsc{MBSS}]
		\begin{tabular}[h]{l p{0.75\textwidth}}
			\textsc{Input:} & Signed graph $G$ and weights $w(\dif{e})$ for each \dif{edge $e$}. \\
			\textsc{Objective:} & Find $B \subset \dif{E}(G)$ that \dif{spans} a balanced subgraph
			and that maximizes $\sum_{e \in B} w(\dif{e})$,
		\end{tabular}
	\end{problema}
	\begin{minipage}[h][12em][c]{\textwidth}
		\footnotesize \centering%
		\only<2->{
			\input{figures/bss-ex-sol.tex}
			\captionof{figure}{Instance of 2-MBSS with all weights equal to 1.}%
		}
		% \label{fig:bis-instance-sol}
	\end{minipage}
\end{frame}

\begin{frame}{Introduction}{Characterization}
	\only<1->{
		\begin{definition}[Negative cycle]
			A cycle $C \subset G$ is \textit{negative} if
			it has an odd number of negative edges,
			otherwise, it is \textit{positive}.
	}
		\end{definition}
	\only<2->{
		\begin{teo}[\cite{harary1953}]
			A signed graph is 2-balanced if and only if
			it does not have a negative cycle as subgraph.
		\end{teo}
	}
\end{frame}

\begin{frame}{Formulations}{2-MBIS}
	\textsc{2-MBIS} formulation:
	\begin{align}
		\max  &\qquad \m w^\tr \m x \\
		\st &\qquad \sum_{u \in V(C)} \m x_u \leq |C| - 1,\qquad\forall C \in \mathcal{C}^- \\
			&\qquad \m x \in \{0,1\}^{n(G)}
	\end{align}\\[2em]
	\parbox{\textwidth}{
		\centering
		\alert{$\mathcal{C}^- := \{C \subset G : C \text{ is a chordless negative cycle}\}$}
	}
\end{frame}

\section{Formulations}
\begin{frame}{Formulations}{2-MBSS}
	\textsc{2-MBSS} formulation:
	\begin{align}
		\max  &\qquad \m w^\tr \m x \\
		\st &\qquad \sum_{\dif{e} \in \dif{E}(C)} \m x_{\dif{e}} \leq |C| - 1,\qquad\forall C \in \mathcal{C}^- \\
			&\qquad \m x \in \{0,1\}^{\dif{m}(G)}
	\end{align}\\[2em]
	\parbox{\textwidth}{
		\centering
		\alert{$\mathcal{C}^- := \{C \subset G : C \text{ is a negative cycle}\}$}
	}
\end{frame}

\section{Relationship between 2-MBIS and 2-MBSS}

\begin{frame}{Relationship between 2-MBIS and 2-MBSS}{}
	An instance $G$ of 2-MBSS can be reduced to an instance $G^*$ of 2-MBIS:
	\parbox[h]{\textwidth}{\raggedleft \small $G$: \input{figures/bss-ex-sm.tex}}

	\begin{minipage}[h][12em][c]{\textwidth}
		\centering%
		\vspace{3em}
		\input{figures/bss-to-bis.tex}
		\captionof{figure}{%
			\only<1>{Instance of 2-MBSS.}
			\only<2>{Intermediate step.}
			\only<3->{Equivalent instance $G^*$ of 2-MBIS.}
		}%
		\label{fig:bss-to-bis}
	\end{minipage}
\end{frame}



\section{Previous results}
\begin{frame}{Previous results}{2-MBIS}
	\begin{block}{}
		\alert{On the notion of balance of a signed graph.}\nocite{harary1953}\\
		\textbf{Harary, F.}
		\textit{Michigan Mathematical Journal,} 1953.
	\end{block}
	\begin{block}{}
		\alert{Facets of the balanced (acyclic) induced subgraph polytope.}\nocite{barahona1989}\\
		\textbf{Barahona, F. e Mahjoub, A. R.}
		\textit{Mathematical Programming,} 1989.
	\end{block}
	\begin{block}{}
		\alert{An exact approach to the problem of extracting an embedded network matrix.}
		\nocite{figueiredo2011}\\
		\textbf{Figueiredo, R. M., Labbé, M., e Souza, C. C. de.}\\
		\textit{Computers \& Operations Research,} 2011.
	\end{block}
\end{frame}

\begin{frame}{Previous results}{Applications}
	\begin{block}{}
		\alert{Signed graphs for portfolio analysis in risk management.}\\%\nocite{harary2002}\\
		\textbf{Harary, F., Lim, M.-H., e Wunsch, D. C.}\\
		\textit{IMA Journal of Management Mathematics,} 2002.
	\end{block}
	\begin{block}{}
		\alert{Correlation Clustering.}\\%\nocite{bansal2004}\\
		\textbf{Bansal, N., Blum, A., e Chawla, S.}
		\textit{Machine Learning,} 2004.
	\end{block}
	\begin{block}{}
		\alert{Algorithmic and complexity results for decompositions of biological networks
		into monotone subsystems.}\\%\nocite{dasgupta2007}\\
		\textbf{DasGupta, B., Enciso, G. A., Sontag, E., e Zhang, Y.}\\
		\textit{Biosystems,} 2007.
	\end{block}
	\begin{block}{}
		\alert{Balanced portfolio via signed graphs and spectral clustering
		in the brazilian stock market.}\\%\nocite{mansano2022}\\
		\textbf{Mansano, R., Allem, L. E., Del Vecchio, R., e Hoppen, C.}\\
		\textit{Quality \& Quantity,} 2022.
	\end{block}
\end{frame}

\section{Contribuitions}

\begin{frame}{Contributions}{Covering formulation}
	\begin{minipage}[h][15em][c]{\textwidth}
		\begin{enumerate}
			\item[1] Formulação de cobertura para o \textsc{BIS}.
		\end{enumerate}
		\begin{align}
			\min  &\qquad \m w^\tr \m y \\
			\st &\qquad \sum_{u \in C} \m y_u \geq 1,\qquad\forall C \in \mathcal{C}^- \\
				&\qquad \m y \in \{0,1\}^{n(G)}
		\end{align}
		\begin{theorem}
			\begin{minipage}[h][][c]{\textwidth}\centering%
				$\m a^\tr \m x \leq a_0$ defines a facet of the classical formulation
			\end{minipage}
			\parbox[c]{\textwidth}{\centering $\iff$}
			\begin{minipage}[h][][c]{\textwidth}\centering%
				$\m a^\tr \m y \geq \m{e}^\tr\m{a} - a_0$
				is facet defining inequality of the covering formulation.
			\end{minipage}

		\end{theorem}
	\end{minipage}
\end{frame}

\begin{frame}{Contribuitions}{2-MBIS}
	Let $\mathcal{P}(G,S)$ be the 2-BIS polytope of $(G,S)$.
	\begin{enumerate}
	\setcounter{enumi}{1}
		\item $\mathcal{N}$-set inequalities:
			definition and sufficiency to be facet defining.\\[2em]

		\item Operations: 
			\begin{itemize}
				\item Vertex deletion: $\mathcal{P}(G-v,S) \cong \{y \in \mathcal{P}(G,S): y_v = 1\}$\\[1em]
				\item Vertex contraction: $\mathcal{P}(G/v,S) \cong \{y \in \mathcal{P}(G,S): y_v = 0, y_i = 1\ \forall i \in N^{\pm}(v)\}$\\[1em]
				\item Redundant edge deletion: $\mathcal{P}(G-e,S) \cong \mathcal{P}(G,S)$\\[1em]
				\item Signature exchange: $\mathcal{P}(G,S) \cong \mathcal{P}(G,S \triangle C)$, where $C \in E(G)$ is a cut.
			\end{itemize}

		\item Lifting procedures obtained by deleting and/or contracting vertices.

	\end{enumerate}
\end{frame}

\section{What to do now}
\begin{frame}{What to do now}{}
	\begin{enumerate}
		\item Find more classes of facets:
			apply the lifting procedures or $\mathcal{N}$-set inequalities for subgraphs to find new facets.\\[2em]

		\item Find weaker necessary conditions so that $\mathcal{N}$-set inequalities are facet defining. \\[2em]

		\item Develop/Implement cutting planes using the new facets found.

		\item Explore the equivalency between 2-MBIS and 2-MBSS in classes of graphs, algorithms and polyhedral studies.

	\end{enumerate}
\end{frame}

\section{References}
% \begin{frame}[allowframebreaks]{Referências}
\begin{frame}{References}
	\footnotesize
	\nocite{campelo2008}
	\bibliography{referencias}
	\bibliographystyle{apalike}
\end{frame}

% {\1
% \begin{frame}[plain,noframenumbering]
  % \finalpage{Obrigado.}
% \end{frame}}

\end{document}

