# Maximum k-balanced induced (spanning) subgraph

Presentation for CNPq Inter Project called *optimization problems in signed graphs
with applications in social networks*.

## Abstract
A signed graph $G$ is a graph associated with a *sign* function,
which maps each edge of $G$ to a positive or negative sign.
Thus, the edge set $E$ of $G$ can be partitioned in two disjoint sets $E^+$ and $E^-$,
that correspond to the positive and negative edges of $G$, respectively.
We allow multiple edges in G as long as they have different signs.
% Let $E(S)$, for $S \subset V$, be the set of edges with both endpoints in S.
We say that a signed graph is balanced if $V$ can be partitioned in $U$ and $\bar{U}$
in such a way that all negative edges have one endpoint in $U$
and the other one in $\bar{U}$,
and all positive edges have both endpoints in the same partition,
that is there is no positive edge in the cut defined by $U$ and $\bar{U}$.
The ***Balanced Induced Subgraph Problem*** (BIS) consists in,
given a signed graph $G$ with weights for each vertex,
finding a balanced induced subgraph of $G$ that
maximizes the sum of weights of its vertices.
The balanced induced subgraph polytope of $G$, namely $\mathcal{P}_G$, consists of
the convex hull of its balanced induced subgraphs' vertex-incidence vectors.
Using an affine transformation from $\mathcal{P}_G$ to a set covering polytope,
we investigate relations between facets from one polytope to the other.

**Keywords**: graph theory; signed graph; balanced induced subgraph; polyhedral combinatorics.

## Resumo

Um grafo de sinal $G$ é um grafo associado a uma função de sinal que mapeia cada aresta
de $G$ em um sinal positivo ou negativo. Dessa forma, o conjunto E de arestas de $G$ pode
ser particionado em dois conjuntos disjuntos $E^+$ e $E^-$, que correspondem as arestas
positivas e negativas, respectivamente. É permitido que hajam arestas múltiplas
contanto que tenham sinais diferentes. Um grafo de sinal é balanceado se V pode ser
particionado em $U$ e $\bar{U}$ de tal forma que as arestas negativas tenham uma extremidade
em $U$ e a outra em $\bar{U}$ e as arestas positivas possuam as duas extremidades na mesma
partição.
O *Problema do Subgrafo Induzido Balanceado* (BIS) consiste em, dado um grafo $G$ com
custos associados a cada vértice, encontrar um subgrafo induzido balanceado que
maximiza a soma dos custos dos vértices que contém. O politopo de subgrafo induzido
balanceado de $G$, denotado $P_G$, é definido pelo fecho convexo dos vetores de incidência
de vértices de todos os subgrafos induzidos balanceados de $G$. A partir de uma
transformação afim de $P_G$ em um politopo de cobertura, estudamos a relação das facetas
desses dois politopos, e classificamos as facetas conhecidas de $P_G$ em duas classes de
facetas. As classes obtidas podem ser generalizadas para outros problemas de cobertura,
como o *Feedback Vertex Set*, entre outros.

**Palavras-chave**: teoria dos grafos; facetas de poliedros; programação inteira; grafos de sinal

